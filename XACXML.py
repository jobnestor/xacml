#!/usr/bin/env python3#!/usr/bin/env python3
#This assumes that the given policies are already uploaded for the requests one wants to send
#All this programme does is to check if everything is formatted correctly, and then sends and access request and gets a response

import os
import sys
from ndg import *
from lxml import etree
from bs4 import BeautifulSoup
import requests

app.config.from_pyfile('config.py')

#Checks if the content of the passed XACML is correct according to schema
def checkCorrectXACML(xml_path: str, xsd_path: str) -> bool:
    input_schema = etree.parse(xsd_path)
    schema = etree.XMLSchema(input_schema)
    request = etree.parse(xml_path)
    result = schema.validate(request)
    return result

def checkPolicyCompliance(request, xmlns):
    requestStatus = False
    if(xmlns == 'urn:oasis:names:tc:xacml:2.0:context:schema:os'):
        print("using schema \'urn:oasis:names:tc:xacml:2.0:context:schema:os\'")
        try:
            policydir = f"{app.config['POLDIR']}/xacmlpol2.xsd"
            checkCorrectXACML(request, policydir)
            requestStatus = True
            print("request is valid")
        except:
            print("Invalid XACML formatting")
            quit()
    elif(xmlns == 'urn:oasis:names:tc:xacml:1.0:context'):
        print("using schema \'urn:oasis:names:tc:xacml:1.0:context\'")
        try:
            policydir = f"{app.config['POLDIR']}/xacmlpol1.xsd"
            checkCorrectXACML(request, policydir)
            requestStatus = True
            print("request is valid")
        except:
            print("Invalid XACML formatting")
            quit()

    return requestStatus

#Reads the request from the given file and checks which policy to apply
def readRequest():
    request = str(sys.argv[1])
    raw_data = open(request, 'r')
    data = raw_data.read() 
    tag = BeautifulSoup(data, 'xml').Request
    xmlns = tag['xmlns']
    print(f'found namespace: ', xmlns)

    if(checkPolicyCompliance(request, xmlns) == True):
        return data
    else:
        raw_data.close
        raise Exception("Unknown policy")

def receive_request(request):
    desicion = 'Deny'
	basepath = '/authzforce-ce/domains/'
	baseurl = f"{app.config['IP']}:{app.config['PORT']}{basepath}"
	d = requests.get(baseurl)
	domain = d.text
	url = (baseurl+domain)+'/pdp'
    r = requests.post(url, xml=request)
	desicion = r.text
    return desicion


def checkAccessPolicy(request):
    accessDecision = False
    accessDecision = receive_request(request)
    desicionTag = BeautifulSoup(accessDecision, 'xml').Desicion
    accessDecision = desicionTag.text
    return accessDecision

def main():
    if(len(sys.argv) == 2):
        request = readRequest()
        print(checkAccessPolicy(request))

    else:
        raise Exception('You need to provide a request path')


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
